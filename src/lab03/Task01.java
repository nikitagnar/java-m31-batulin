package lab03;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите сторону квадрата: ");
        double squ = input.nextDouble();

        System.out.println("Длина 2 стороны прямоугольника: ");
        double rec1 = input.nextDouble();
        double rec2 = input.nextDouble();

        System.out.println("Длина массива(треугольник): ");
        int sizeTriangle = input.nextInt();
        double arrayTriangle[] = new double[sizeTriangle];
        System.out.println("Введите элементы массива(треугольник):");
        for (int i = 0; i < sizeTriangle; i++) {
            arrayTriangle[i] = input.nextDouble();
        }
        Figure[] sq = new Figure[3];
        sq[0] = new Square(squ);
        sq[1] = new Rectangle(rec1, rec2);
        sq[2] = new Triangle(arrayTriangle);
        System.out.println(Figure.summa(sq));
    }
}
