package lab03;

class Rectangle extends Figure {
    double a;
    double b;

    public Rectangle(double rec1, double rec2) {
        this.a = rec1;
        this.b = rec2;
    }


    public double calcPerimeter() {
        return a * 2 + b * 2;
    }

    public double calcArea() {
        return a * b;
    }
}
