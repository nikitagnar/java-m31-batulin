package lab03;

class Library {

    private final Book[] books;
    private int countBooks;

    public Library(int numberOfBooks) {
        books = new Book[numberOfBooks];
    }

    public int countPages() {
        int sumPages = 0;
        for (Book book : books) {
            sumPages += book.getPages();
        }
        return sumPages;
    }

    public double countAverage() {
        int countPagesOfAllBooks = countPages();
        return countPagesOfAllBooks / countBooks;
    }

    public void addBook(Book bookPages) {
        if (countBooks >= books.length) {
            return;
        }
        books[countBooks] = bookPages;
        countBooks++;
    }
}
