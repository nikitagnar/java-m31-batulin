package lab03;

import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите кол-во книг: ");
        int cBooks = scan.nextInt();
        Library allBooks = new Library(cBooks);
        for (int i = 0; i < cBooks; i++) {
            String a = String.format("Введите кол-во страниц для книги №%d: ", i+1);
            System.out.print(a);
            allBooks.addBook(new Book(scan.nextInt()));
        }
        System.out.println("Общее количество страниц: " + allBooks.countPages());
        System.out.println("Среднее количество страниц: " + allBooks.countAverage());
    }
}

