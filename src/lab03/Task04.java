package lab03;

import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите радиус 1-ой окружности: ");
        double r1 = scan.nextDouble();
        Circle c1 = new Circle(r1);
        System.out.print("Введите радиус 2-ой окружности: ");
        double r2 = scan.nextDouble();
        Circle c2 = new Circle(r2);
        System.out.println("Площадь окружности: " + c1.circleArea());
        c1.position(1, 2);
        if (c1.enter(c2) == true) {
            System.out.println("Окружность входит в другую");
        } else {
            System.out.println("Окружность не входит в другую");
        }
        System.out.println("Расстояние между центрами окружностей: " + c1.distance(c2));
    }
}

