package lab03;

class Circle {
    private double r;
    private double x = 0;
    private double y = 0;

    public Circle(double r) {
        this.r = r;
    }

    public double circleArea() {
        return Math.PI * Math.pow(r, 2);
    }

    public void position(double deltax, double deltay) {
        this.x += deltax;
        this.y += deltay;
    }

    public boolean enter(Circle c2) {
        return (Math.pow((this.x - c2.x), 2) + Math.pow((this.y - c2.y), 2)) <= Math.pow((this.r - c2.r), 2);
    }

    public double distance(Circle c2) {
        return Math.sqrt(Math.pow((this.x - c2.x), 2) + Math.pow((this.y - c2.y), 2));
    }
}

