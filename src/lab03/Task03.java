package lab03;

import java.util.Scanner;

public class Task03 {
    static int requestInt() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите число: ");
        return scan.nextInt();
    }


    public static void main(String[] args) {
        int n = requestInt();
        System.out.print("Результат: ");
        System.out.println(fibonacci(n));
    }

    static int fibonacci(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

