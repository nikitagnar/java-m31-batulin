package lab04;

public class Engine {
    private final double power;
    private final String prod;

    Engine(double power, String prod) {
        this.power = power;
        this.prod = prod;
    }

    public double getPower() {
        return power;
    }

    public String getProd() {
        return prod;
    }
}
