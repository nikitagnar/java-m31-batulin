package lab04;

public class Lorry extends Car {
    private final int capacity;

    Lorry(String brand, String autoClass, int weight, Driver driver, Engine engine, int capacity) {
        super(brand, autoClass, weight, driver, engine);
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }
}

