package lab04;

import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите ФИО и стаж водителя: ");
        Driver driver = new Driver(scan.nextLine(), scan.nextInt());
        System.out.println("Введите мощность и производителя машины: ");
        Engine engine = new Engine(scan.nextDouble(), scan.nextLine());
        System.out.println("Введите марку, класс и вес машины: ");
        Car car = new Car(scan.nextLine(), scan.nextLine(), scan.nextInt(), driver, engine);
        System.out.println(car.toString());
        car.start();
        car.turnRight();
        car.turnLeft();
        car.stop();
    }
}
