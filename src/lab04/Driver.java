package lab04;

public class Driver extends Person {
    private final int experience;

    Driver(String name, int experience) {
        super(name);
        this.experience = experience;
    }

    public int getExperience() {
        return experience;
    }
}

