package lab04;

public class SportCar extends Car {
    private final int maxSpeed;

    SportCar(String brand, String autoClass, int weight, Driver driver, Engine engine, int maxSpeed) {
        super(brand, autoClass, weight, driver, engine);
        this.maxSpeed = maxSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }
}
