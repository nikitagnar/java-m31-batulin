package lab04;

public class Car {
    private final String brand;
    private final String autoClass;
    private final int weight;
    private final Driver driver;
    private final Engine engine;

    Car(String brand, String autoClass, int weight, Driver driver, Engine engine) {
        this.brand = brand;
        this.autoClass = autoClass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }

    public static void start() {
        System.out.println("Поехали");
    }

    public static void stop() {
        System.out.println("Останавливаемся");
    }

    public static void turnRight() {
        System.out.println("Поворот направо");
    }

    public static void turnLeft() {
        System.out.println("Поворот налево");
    }

    @Override
    public String toString() {
        return "ФИО водителя: " + driver.getName() + ". Стаж вождения: " + driver.getExperience() + ". Мощность авто: " +
                engine.getPower() + ". Производитель авто: " + engine.getProd() + ". Марка авто: " + brand +
                ". Класс авто: " + autoClass + ". Вес авто: " + weight;
    }
}
