package lab08;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        try (FileWriter fw = new FileWriter("src/lab08/num.txt", true)) {
            int a = requestNumber("Введите начало диапазона положительных чисел: ");
            int b = requestNumber("Введите конец диапазона положительных чисел: ");
            for (int i = 0; i < 50; i++) {
                int c = a + (int) (Math.random() * b);
                fw.write(c + " ");
            }
            a = requestNumber("Введите начало диапазона отрицательных чисел: ");
            b = requestNumber("Введите конец диапазона отрицательных чисел: ");
            for (int i = 0; i < 50; i++) {
                int c = a + (int) (Math.random() * b);
                fw.write(c + " ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (FileReader fr = new FileReader("src/lab08/num.txt");
             FileWriter fwp = new FileWriter("src/lab08/positive_num.txt", true);
             FileWriter fwn = new FileWriter("src/lab08/negative_num.txt", true)) {
            Scanner scan = new Scanner(fr);
            String numbers = scan.nextLine().trim();
            String[] arr = numbers.split(" ");
            int[] numbersArr = new int[arr.length];
            for (int i = 0; i < arr.length; i++) {
                numbersArr[i] = Integer.parseInt(arr[i]);
            }
            for (int num : numbersArr) {
                if (num >= 0) {
                    fwp.write(num + " ");
                } else {
                    fwn.write(num + " ");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static int requestNumber(String str) {
        Scanner scan = new Scanner(System.in);
        System.out.print(str);
        return scan.nextInt();
    }
}
