package lab02;

import java.util.Scanner;

public class Task24 {
    public static void main(String[] args) {
        Scanner scan1 = new Scanner(System.in);
        int n = scan1.nextInt();
        String[] myArray = new String[n];
        for (int i = 0; i < n; i++) {
            Scanner scan2 = new Scanner(System.in);
            String elem = scan2.nextLine();
            myArray[i] = elem;
        }
        System.out.println(String.join(", ", myArray));
    }
}
