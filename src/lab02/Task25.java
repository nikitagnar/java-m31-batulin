package lab02;

import java.util.Scanner;

public class Task25 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String message = scan.nextLine();
        if (message.toUpperCase().contains("ЖЫ") || message.toUpperCase().contains("ШЫ")) {
            System.out.println("В тексте присутствует ошибка жи/ши");
        } else {
            System.out.println("В тексте нет ошибки жи/ши");
        }
    }
}
