package lab02;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int size = scan.nextInt();
        int a = 0;
        int b = 1;
        for (int i = 0; i < size; i++) {
            int elem = scan.nextInt();
            if (elem % 2 == 0) {
                a += elem;
            } else {
                b *= elem;
            }
        }
        System.out.print("Сумма чётных: ");
        System.out.println(a);
        System.out.print("Произведение нечётных: ");
        System.out.println(b);
    }
}
