package lab07;

public class BinaryTree<T extends Comparable<T>> {
    private Node<T> root;

    protected static class Node<T> {
        protected T inf;
        Node<T> left;
        Node<T> right;

        protected Node(Node<T> left, Node<T> right, T inf) {
            this.inf = inf;
            this.left = left;
            this.right = right;
        }

    }

    public BinaryTree() {
        this.root = null;
    }

    public void add(T inf) {
        if (root == null) {
            root = new Node<T>(null, null, inf);
            return;
        }
        add(inf, root);
    }

    private void add(T inf, Node<T> head) {
        if (inf.compareTo(head.inf) == 0) {
            return;
        }
        if (inf.compareTo(head.inf) > 0) {
            if (head.right != null) {
                add(inf, head.right);
            } else {
                head.right = new Node<T>(null, null, inf);
            }
        } else {
            if (head.left != null) {
                add(inf, head.left);
            } else {
                head.left = new Node<T>(null, null, inf);
            }
        }
    }

    public void remove(T inf) {
        if (root == null) {
            return;
        }
        root = remove(inf, root);
    }

    private Node<T> remove(T inf, Node<T> head) {
        if (inf.compareTo(head.inf) > 0) {
            head.right = remove(inf, head.right);
            return head;
        }
        if (inf.compareTo(head.inf) < 0) {
            head.left = remove(inf, head.left);
            return head;
        }
        if (head.left == null) {
            return head.right;
        }
        if (head.right == null) {
            return head.left;
        }
        T minValue = findMin(head.right).inf;
        head.inf = minValue;
        head.right = remove(minValue, head.right);
        return head;
    }

    private Node<T> findMin(Node<T> head) {
        if (head.left == null) return head;
        return findMin(head.left);
    }

    public void printTree() {
        printTree(root);
        System.out.println();
    }

    private void printTree(Node<T> head) {
        if (head.left != null) {
            printTree(head.left);
        }
        System.out.print(head.inf + " ");
        if (head.right != null) {
            printTree(head.right);
        }
    }
}
